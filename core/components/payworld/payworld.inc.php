<?php

$modx->addPackage(
    'shopkeeper',
    $modx->getOption('core_path').'components/shopkeeper/model/'
);


    switch ($_REQUEST['action'])
    {
        case 'fail':
        case 'success':
            $output = $_REQUEST['result_message'];
            break;
            
        case 'callback':
            if (isset($_REQUEST['transaction_id']) 
                && isset($_REQUEST['order_id']) 
                && isset($_REQUEST['order_total'])
                && isset($_REQUEST['payer_email']) 
                && isset($_REQUEST['seller_name']) 
                && isset($_REQUEST['shop_id'])
                && isset($_REQUEST['hash']))
            {
                $email_on_error =
                    $modx->getOption(
                        'EMAIL_ON_ERROR',
                        $scriptProperties,
                        null
                );

                $secret_code = 
                    $modx->getOption(
                        'SECRET_CODE',
                        $scriptProperties,
                        null
                );

                $hash = md5(
                      $_REQUEST['order_id'] 
                    . $_REQUEST['order_total'] 
                    . $_REQUEST['transaction_id'] 
                    . $_REQUEST['payer_email'] 
                    . $_REQUEST['seller_name'] 
                    . $_REQUEST['shop_id'] 
                    . $secret_code
                );
                
                if ($_REQUEST['hash'] == $hash)
                {
                    $order = 
                        $modx->getObject(
                            'SHKorder', 
                            $_REQUEST['order_id']
                        );
                
                    if ($order)
                    {
                        $order->set('status',5);
                        $order->save();
                        $output = 'Success';
                    }
                    else
                    {
                        notify_by_email(
                            $email_on_error, 
                            'Ошибка. Заказ не найден.', 
                            'Поступила оплата заказа №' 
                                . $_REQUEST['order_id'] 
                                . ' на сумму '
                                . $_REQUEST['order_total']
                                . ' руб. '
                                . ' Заказ с таким номером не найден!'
                        );

                        header('HTTP/1.1 500 Internal Server Error');
                        die('No order found');
                    }
                }
                else
                {
                    notify_by_email(
                        $email_on_error, 
                        'Ошибка. Попытка подмены данных об оплате.', 
                        'Поступили данные об оплате с неверным хэшем:'
                            . ' заказ №'
                            . $_REQUEST['order_id'] 
                            . ' на сумму '
                            . $_REQUEST['order_total']
                            . ' руб. '
                    );

                    header('HTTP/1.1 500 Internal Server Error');
                    die('Wrong hash');
                }
            }
            else
            {
                header('HTTP/1.1 500 Internal Server Error');
                die('Not all parameters');
            }
            break;

        case 'payment':
        default:
            if ($_SESSION['shk_payment_method'] == 'payworld')
            {
                /* Считываем параметры */
                $server = $scriptProperties['SERVER'];
                $seller_name = $scriptProperties['SELLER_NAME'];
                $shop_id = $scriptProperties['SHOP_ID'];
                $button_text = $scriptProperties['BUTTON_TEXT'];

                $order_id = $_SESSION['shk_order_id'];
                $amount = number_format(
                    $_SESSION['shk_order_price'], 2, '.', ''
                );

                /* Формируем детали заказа */
                $order = 
                    $modx->getObject('SHKorder', $order_id);
                    
                require_once MODX_CORE_PATH 
                    ."components/shopkeeper/model/shopkeeper.class.php";
                require_once MODX_CORE_PATH
                    ."components/shopkeeper/model/shk_mgr.class.php";
                require_once MODX_CORE_PATH
                    ."components/shopkeeper/include.parsetpl.php";

                $SHKmanager = new SHKmanager($modx);
                $SHKmanager->getModConfig();
                $SHKmanager->config['tplPath'] = 
                    'core/components/payworld/elements/chunks/';
                $SHKmanager->config['orderDataTpl'] = 'order_info';

                $purchases = unserialize($order->get('content'));
                $addit_params = unserialize($order->get('addit'));
                $orderData = $SHKmanager->getOrderData(
                    $purchases, $addit_params);
                $order_details = parseTpl(
                    '@INLINE '.$orderData,
                    array('orderID'=>$order_id, 'date'=>$date)
                );

                /* Статус заказа: принят к оплате */
                $order = $modx->getObject('SHKorder', $order_id);
                $order->set('status', 1);
                $order->save();

                /* Форма с одной кнопкой */
                $output  = "<form action='".$server."' method='post'>";
                $output .= "<input type='hidden' name='order_id' "
                    ."value='".$order_id."'>";
                $output .= "<input type='hidden' name='order_total' "
                    ."value='".$amount."'>";
                $output .= "<input type='hidden' name='order_details' "
                    ."value='".$order_details."'>";
                $output .= "<input type='hidden' name='seller_name' "
                    ."value='".$seller_name."'>";
                $output .= "<input type='hidden' name='shop_id' "
                    ."value='".$shop_id."'>";
                $output .= "<input type='submit' name='submit' "
                    ."value='".$button_text."'>";
            }
            break;
    }

    return $output;



    function notify_by_email($to, $subject, $message)
    {
        if ($to)
        {
            $headers  = "From: \"Payworld Error Notifier\" " 
                . "<no-reply@" . $_SERVER['HTTP_HOST'] . ">\r\n"
                . "MIME-Version: 1.0\r\n"
                . "Content-type: text/plain; charset=utf-8\r\n"
                . "Content-Transfer-Encoding: 8bit";

            mail(
                $to,
                "=?utf-8?B?" . base64_encode($subject) . "?=",
                $message,
                $headers
            );
        }
    }

?>
